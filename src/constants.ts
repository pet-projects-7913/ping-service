// Target site url
export const targetUrl = process.env.TARGET_URL || 'https://fundraiseup.com/';
// Server api url
export const serverApiControllerUrl = process.env.SERVER_API_CONTROLLER_URL || '/data';
// Wait time in milliseconds
export const waitTime = process.env.WAIT_TIME ? parseFloat(process.env.WAIT_TIME) : 10000;
// Ping time in milliseconds
export const pingPeriod = process.env.PING_PERIOD ? parseFloat(process.env.PING_PERIOD) : 1000;
// Delivery attempt delay factor
export const deliveryAttemptDelayFactor = process.env.DELIVERY_ATTEMPT_DELAY_FACTOR
  ? parseFloat(process.env.DELIVERY_ATTEMPT_DELAY_FACTOR) : 2.7;
// Min delivery attempt delay in milliseconds
export const minDeliveryAttemptDelay = process.env.MIN_DELIVERY_ATTEMPT_DELAY
  ? parseFloat(process.env.MIN_DELIVERY_ATTEMPT_DELAY) : 20;
// Total ping time in milliseconds
export const totalPingTime = process.env.TOTAL_PING_TIME ? parseFloat(process.env.TOTAL_PING_TIME) : 30000;
