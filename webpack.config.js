const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');

module.exports = [{
  mode: 'development',
  entry: './src/client/main.ts',
  output: {
    path: path.resolve(__dirname, 'dist', 'public'),
    filename: 'script.js',
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js'],
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        exclude: /node_modules/,
        loader: 'ts-loader',
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'ping-service',
      filename: 'index.html',
    }),
    new webpack.DefinePlugin({
      'process.env.TARGET_URL': JSON.stringify(process.env.TARGET_URL),
      'process.env.SERVER_API_CONTROLLER_URL': JSON.stringify(process.env.SERVER_API_CONTROLLER_URL),
      'process.env.WAIT_TIME': JSON.stringify(process.env.WAIT_TIME),
      'process.env.PING_PERIOD': JSON.stringify(process.env.PING_PERIOD),
      'process.env.DELIVERY_ATTEMPT_DELAY_FACTOR': JSON.stringify(process.env.DELIVERY_ATTEMPT_DELAY_FACTOR),
      'process.env.MIN_DELIVERY_ATTEMPT_DELAY': JSON.stringify(process.env.MIN_DELIVERY_ATTEMPT_DELAY),
      'process.env.TOTAL_PING_TIME': JSON.stringify(process.env.TOTAL_PING_TIME),
    }),
  ],
},
{
  mode: 'development',
  target: 'node',
  entry: './src/server/index.ts',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'server.js',
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js'],
    fallback: {
      fs: false,
      buffer: false,
      http: false,
      path: false,
    },
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        exclude: /node_modules/,
        loader: 'ts-loader',
      },
    ],
  },
  plugins: [
  ],
}];
