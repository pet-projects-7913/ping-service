import DeliveryAttemptStatistic from './DeliveryAttemptStatistic';
import StopPingToken from './StopPingToken';
import launchDeliveryAttemptQueue from './launchDeliveryAttemptQueue';
import measurePing from './measurePing';
import { targetUrl, pingPeriod, deliveryAttemptDelayFactor, minDeliveryAttemptDelay, totalPingTime } from '../constants';

function launchPing(
  targetUrl: string,
  pingPeriod: number,
  minDeliveryAttemptDelay: number,
  deliveryAttemptDelayFactor: number,
  stopPingToken: StopPingToken,
): DeliveryAttemptStatistic {
  const deliveryAttemptStatistic = new DeliveryAttemptStatistic();
  let pingId = 0;
  const pingInterval: ReturnType<typeof setInterval> = setInterval(async () => {
    pingId += 1;
    const responseTimeObject = await measurePing(targetUrl);
    await launchDeliveryAttemptQueue(
      pingId,
      responseTimeObject,
      minDeliveryAttemptDelay,
      deliveryAttemptDelayFactor,
      deliveryAttemptStatistic,
      stopPingToken,
    );
  }, pingPeriod);
  stopPingToken.setPingInterval(pingInterval);
  return deliveryAttemptStatistic;
}

const stopPingToken = new StopPingToken();
const deliveryAttemptStatistic = launchPing(
  targetUrl,
  pingPeriod,
  minDeliveryAttemptDelay,
  deliveryAttemptDelayFactor,
  stopPingToken,
);
setTimeout(() => {
  stopPingToken.stopPing();
  deliveryAttemptStatistic.printReport();
}, totalPingTime);
