module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    'airbnb-base', // plug airbnb rules
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended',
  ],
  plugins: [
    'babel',
    'import',
    'prettier',
    '@typescript-eslint',
  ],
  parserOptions: {
    parser: '@typescript-eslint/parser', // the typescript-parser for eslint, instead of tslint
    sourceType: 'module', // allow the use of imports statements
    ecmaVersion: 2021, // allow the parsing of modern ecmascript
    ecmaFeatures: {
      modules: true,
      experimentalObjectRestSpread: true,
    },
  },
  rules: {
    '@typescript-eslint/no-var-requires': 'warn',
    'no-param-reassign': 'warn',
    'no-restricted-syntax': 'warn',
    'class-methods-use-this': 'off',
    'no-shadow': 'off',
    'no-empty': 'warn',
    'no-tabs': 'warn',
    camelcase: 'warn',
    'global-require': 'warn',
    'import/extensions': 'off',
    'no-await-in-loop': 'warn',
    'no-unsafe-finally': 'warn',
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'linebreak-style': 'off', // Неправильно работает в Windows.
    'no-unused-expressions': 'warn',
    'no-trailing-spaces': 'warn',
    indent: 'warn',
    quotes: 'warn',
    'comma-dangle': 'warn',
    'no-extraneous-dependencies': 'off',
    'no-const-assign': 'warn',
    'no-this-before-super': 'warn',
    'import/no-unresolved': 'off',
    'no-undef': 'warn',
    'no-unreachable': 'warn',
    'no-unused-vars': 'warn',
    'constructor-super': 'warn',
    'valid-typeof': 'warn',
    'lines-between-class-members': 'warn',
    semi: 'warn',
    'arrow-parens': 'off', // Несовместимо с prettier
    'object-curly-newline': 'off', // Несовместимо с prettier
    'no-mixed-operators': 'off', // Несовместимо с prettier
    'arrow-body-style': 'off',
    'function-paren-newline': 'off', // Несовместимо с prettier
    'no-plusplus': 'off',
    'space-before-function-paren': 0, // Несовместимо с prettier
    'max-len': ['off', { ignoreTemplateLiterals: true, ignoreStrings: true }],

  },
  settings: {
    'import/resolver': {
      node: {
        extensions: ['.js', '.jsx', '.ts', '.tsx', '.vue'],
        paths: ['components', 'layouts'],
      },
    },
  },
};
