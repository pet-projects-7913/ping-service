import { IncomingMessage, Server, ServerResponse } from 'http';
import { Buffer } from 'buffer';
import { PingAttempt } from '~/src/api/client.types';

const http = require('http');
const path = require('path');
const fs = require('fs').promises;

const PingStorage = require('./PingStorage');

const globalWithPingLog = global as typeof globalThis & {
    pingStorage: PingStorage;
};
globalWithPingLog.pingStorage = new PingStorage();

const server: Server = http.createServer(async (req: IncomingMessage, res: ServerResponse) => {
  if (req.method === 'GET') {
    // Serve static files
    if (req.url === '/') {
      res.writeHead(200, {
        'Content-Type': 'text/html; charset=utf-8',
      });
      try {
        const content: string | Buffer = await fs.readFile(path.join(__dirname, 'public', 'index.html'), 'utf-8');
        res.end(content);
      } catch (e) {
        res.writeHead(500, {
          'Content-Type': 'application/json; charset=utf-8',
        });
        res.end(JSON.stringify(e));
      }
    } else if (req.url === '/script.js') {
      res.writeHead(200, {
        'Content-Type': 'application/javascript; charset=utf-8',
      });
      try {
        const content: string | Buffer = await fs.readFile(path.join(__dirname, 'public', 'script.js'), 'utf-8');
        res.end(content);
      } catch (e) {
        res.writeHead(500, {
          'Content-Type': 'application/json; charset=utf-8',
        });
        res.end(JSON.stringify(e));
      }
    }
  } else if (req.method === 'POST') {
    // Fetch ping data
    if (req.url === '/data') {
      // Generate random response with given probabilities
      const randomValue: number = Math.random();
      const errorProbability: number = process.env.ERROR_PROBABILITY ? parseFloat(process.env.ERROR_PROBABILITY) : 0.2;
      const stuckProbability: number = process.env.STUCK_PROBABILITY ? parseFloat(process.env.STUCK_PROBABILITY) : 0.2;
      const successProbability: number = 1 - errorProbability - stuckProbability;
      if (randomValue < successProbability) {
        res.writeHead(200, {
          'Content-Type': 'text/json',
        });

        const buffers: Buffer[] = [];
        for await (const chunk of req) {
          buffers.push(chunk);
        }
        const data: string = Buffer.concat(buffers).toString();
        const parsedData: PingAttempt = JSON.parse(data);
        console.log(data);
        if (parsedData?.responseTime) {
          globalWithPingLog.pingStorage.addPing(parsedData.responseTime);
        }
        res.end();
      } else if (randomValue < successProbability + errorProbability) {
        res.writeHead(500, {
          'Content-Type': 'text/json',
        });
        res.end();
      } else {
        res.writeHead(200, {
          'Content-Type': 'text/json',
        });
      }
    }
  }
});

const serverStopCallback = function () {
  console.log(`Average ping: ${globalWithPingLog.pingStorage.getAveragePing()}`);
  console.log(`Median ping: ${globalWithPingLog.pingStorage.getMedianPing()}`);
  console.log('Closing http server...');
  server.close(() => {
    console.log('Http server closed.');
  });
};

server.listen(8080, () => {
  globalWithPingLog.pingStorage.clear();
  console.log('Server is running on http://localhost:8080...');
});

process.on('SIGTERM', serverStopCallback);
process.on('SIGINT', serverStopCallback);
