export default class StopPingToken {
    cancel: boolean;

    pingInterval: ReturnType<typeof setInterval> | null;

    constructor() {
      this.cancel = false;
      this.pingInterval = null;
    }

    setPingInterval(pingInterval: ReturnType<typeof setInterval>) {
      this.pingInterval = pingInterval;
    }

    stopPing(): void {
      if (this.pingInterval) {
        clearTimeout(this.pingInterval);
      }
      this.cancel = true;
    }
}
