export default class DeliveryAttemptStatistic {
    successNumber: number;

    errorNumber: number;

    stuckNumber: number;

    constructor() {
      this.successNumber = 0;
      this.errorNumber = 0;
      this.stuckNumber = 0;
    }

    increaseSuccessNumber() {
      this.successNumber += 1;
    }

    increaseErrorNumber() {
      this.errorNumber += 1;
    }

    increaseStuckNumber() {
      this.stuckNumber += 1;
    }

    printReport() {
      console.log(`Total request number: ${this.successNumber + this.errorNumber + this.stuckNumber}.
        Successful request number: ${this.successNumber}.
        Bad request number: ${this.errorNumber}.
        Stuck request number: ${this.stuckNumber}.`);
    }
}
