import { PingAttempt, ResponseTimeObject, Status } from '../api/client.types';
import DeliveryAttemptStatistic from './DeliveryAttemptStatistic';
import StopPingToken from './StopPingToken';
import ServerApiController from './ServerApiController';

export default async function launchDeliveryAttemptQueue(
  pingId: number,
  responseTimeObject: ResponseTimeObject,
  minDeliveryAttemptDelay: number,
  deliveryAttemptDelayFactor: number,
  deliveryAttemptStatistic: DeliveryAttemptStatistic,
  stopPingToken: StopPingToken,
): Promise<void> {
  let status: Status = Status.Stuck;
  let deliveryAttempt = 0;
  let deliveryAttemptDelay: number = minDeliveryAttemptDelay;
  const pause = (duration: number) => new Promise((res) => setTimeout(res, duration));
  while (status !== Status.Success && !stopPingToken?.cancel) {
    deliveryAttempt += 1;
    const pingAttempt: PingAttempt = {
      pingId,
      deliveryAttempt,
      ...responseTimeObject,
    };
    try {
      const serverApiController = new ServerApiController();
      status = await serverApiController.postPing(pingAttempt);
    } finally {
      switch (status) {
        case Status.Success:
          deliveryAttemptStatistic.increaseSuccessNumber();
          break;
        case Status.Stuck:
          deliveryAttemptStatistic.increaseStuckNumber();
          break;
        default:
          deliveryAttemptStatistic.increaseErrorNumber();
      }
      await pause(deliveryAttemptDelay);
      deliveryAttemptDelay *= deliveryAttemptDelayFactor;
    }
  }
}
