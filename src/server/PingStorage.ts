class PingStorage {
    pingLog: number[]

    constructor() {
      this.pingLog = [];
    }

    clear() {
      this.pingLog = [];
    }

    addPing(ping: number) {
      this.pingLog.push(ping);
    }

    getAveragePing(): number {
      if (this.pingLog) {
        return this.pingLog.reduce((accumulator: number, item: number) => accumulator + item, 0) / this.pingLog.length;
      }
      return 0;
    }

    getMedianPing(): number {
      if (this.pingLog) {
        const sortedPingLog: number[] = this.pingLog.sort();
        if (sortedPingLog.length % 2 === 0) {
          return (sortedPingLog[sortedPingLog.length / 2] + sortedPingLog[sortedPingLog.length / 2 - 1]) / 2;
        }
        return sortedPingLog[(sortedPingLog.length - 1) / 2];
      }
      return 0;
    }
}

module.exports = PingStorage;
