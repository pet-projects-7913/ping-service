import axios, { AxiosError, AxiosResponse } from 'axios';
import { PingAttempt, Status } from '../api/client.types';
import { serverApiControllerUrl, waitTime } from '../constants';

export default class ServerApiController {
  logPostPing(status: Status, pingAttempt: PingAttempt, response: AxiosResponse | AxiosError): void {
    console.log(`Status: ${status}. Post message: ${JSON.stringify(pingAttempt)}. Server response: ${JSON.stringify(response)}.`);
  }

  async postPing(pingAttempt: PingAttempt): Promise<Status> {
    const { CancelToken } = axios;
    const source = CancelToken.source();
    let status: Status = Status.Error;
    try {
      setTimeout(() => {
        source.cancel();
        status = Status.Stuck;
      }, waitTime);
      const response: AxiosResponse = await axios.post(serverApiControllerUrl, pingAttempt, {
        cancelToken: source.token,
      });
      status = response.status === 200 ? Status.Success : Status.Error;
      this.logPostPing(status, pingAttempt, response as AxiosResponse);
    } catch (error) {
      this.logPostPing(status, pingAttempt, error as AxiosError);
    }
    return status;
  }
}
