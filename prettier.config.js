module.exports = {
  semi: true,
  trailingComma: 'all',
  singleQuote: true,
  printWidth: 120,
  jsxBracketSameLine: false,
  tabWidth: 4,
};
