export interface PingAttempt {
    'pingId': number,
    'deliveryAttempt': number,
    'date': number,
    'responseTime': number
}

export interface ResponseTimeObject {
    'date': number,
    'responseTime': number
}

export enum Status {
    Success = 'Success',
    Error = 'Error',
    Stuck = 'Stuck',
}
