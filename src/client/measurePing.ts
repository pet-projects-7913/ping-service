import axios from 'axios';
import { ResponseTimeObject } from '../api/client.types';

export default async function measurePing(targetUrl: string): Promise<ResponseTimeObject> {
  const requestStartTime = new Date().getTime();
  try {
    await axios.get(targetUrl);
  } finally {
    const requestEndTime = new Date().getTime();
    const responseTimeObject: ResponseTimeObject = {
      date: requestStartTime,
      responseTime: requestEndTime - requestStartTime,
    };
    return responseTimeObject;
  }
}
